package com.example.artistlookup.Activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.artistlookup.Database.Album_db
import com.example.artistlookup.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_album_detail.*

class Album_Detailes_Activity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "albumInfo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album_detail)
        setupAction()

        setUpDitail()
    }

    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

    }

    private fun setUpDitail() {
        val item = intent.getParcelableExtra<Album_db>(EXTRA_DATA)
        Picasso.with(this).load(item.strAlbumThumb).into(albumImg)
        albumName.text = item.strAlbum
        artistName.text = item.strArtistStripped
        albumDescription.text = item.strDescriptionEN

        toAlbumTrack(item.idAlbum)
    }

    private fun toAlbumTrack(albumId: Int) {
        val intent = Intent(this, Album_tracks_Activity::class.java)
        albumTracks.setOnClickListener {
            intent.putExtra(Album_tracks_Activity.EXTRA_DATA, albumId)
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
