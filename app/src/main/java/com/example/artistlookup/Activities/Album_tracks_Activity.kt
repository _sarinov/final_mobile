package com.example.artistlookup.Activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistlookup.Adapter.AlbumTrackAdapter
import com.example.artistlookup.Networking.Album_Track_Loader
import com.example.artistlookup.R
import kotlinx.android.synthetic.main.activity_album_track.*

class Album_tracks_Activity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "albumTrackInfo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album_track)

        setupAction()


        setupAlbumTrack()
    }

    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }


    private fun setupAlbumTrack() {
        Album_Track_Loader(onSuccess = { it ->
            albumTrackList.layoutManager = LinearLayoutManager(this)
            albumTrackList!!.adapter = AlbumTrackAdapter(albumtrack = it.trackdb, onItemClick = {
                Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()

            })
        }, onError = {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }).loadAlbumTrack(search = intent.getIntExtra(EXTRA_DATA, 1))
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
