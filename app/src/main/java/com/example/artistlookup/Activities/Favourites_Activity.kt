package com.example.artistlookup.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistlookup.Adapter.FavouriteAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.example.artistlookup.Database.Favourites_db
import com.example.artistlookup.R
import kotlinx.android.synthetic.main.activity_favourite.*

class Favourites_Activity : AppCompatActivity() {

    private val auth by lazy {
        FirebaseAuth.getInstance()
    }

    private val database by lazy {
        FirebaseFirestore.getInstance()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        setupAction()

        setupFavourites()
    }

    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

    }

    private fun setupFavourites(){

        favList.layoutManager = LinearLayoutManager(this)
        database.collection("Favourites_Activity").whereEqualTo("uid", auth.currentUser!!.uid)
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                val favs = querySnapshot?.documents?.map {
                    it.toObject(Favourites_db::class.java)
                }
                favList!!.adapter = FavouriteAdapter(favs as List<Favourites_db>,this)
            }
    }


    private fun removeFav(id: Int){
        database.collection("Favourites_Activity").whereEqualTo("aid", id).addSnapshotListener {
                querySnapshot, firebaseFirestoreException ->
                querySnapshot?.documents?.map {
                }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
