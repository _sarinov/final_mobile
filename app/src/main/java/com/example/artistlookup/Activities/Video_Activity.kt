package com.example.artistlookup.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistlookup.Adapter.VideoAdapter
import com.example.artistlookup.R
import com.example.artistlookup.Networking.Video_Info_Loader
import kotlinx.android.synthetic.main.activity_video.*
import java.lang.Integer.parseInt

class Video_Activity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "videos"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        setupVideos()
        setupAction()
    }

    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }



    private fun setupVideos() {
        Video_Info_Loader(onSuccess = { it ->
            videoList.layoutManager = LinearLayoutManager(this)
            videoList!!.adapter = VideoAdapter(
                videoDbs = it.mvids,
                onItemClick ={ webBrowser.loadUrl(it.strMusicVid)}, context =
                this
            )
        }, onError = {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }).loadVideoTrack(parseInt(intent.getStringExtra(EXTRA_DATA)))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
