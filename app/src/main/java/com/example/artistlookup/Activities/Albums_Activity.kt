package com.example.artistlookup.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistlookup.Adapter.AlbumAdapter
import com.example.artistlookup.Database.Album_db
import com.example.artistlookup.R
import com.example.artistlookup.Networking.Album_Info_Loader
import kotlinx.android.synthetic.main.activity_albums.*

class Albums_Activity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "albumInfo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_albums)
        setupAlbums()

        setupAction()

    }

    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupAlbums() {
        Album_Info_Loader(onSuccess = { it ->
            albumList.layoutManager = LinearLayoutManager(this)
            albumList!!.adapter = AlbumAdapter(albumDb = it.albumDb, onItemClick = {
                sendInfotoDetail(it, Intent(this, Album_Detailes_Activity::class.java))
            })
        }, onError = {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }).loadAlbum(intent.getStringExtra(EXTRA_DATA))
    }

    private fun sendInfotoDetail(it: Album_db, intent: Intent) {
        intent.putExtra(
            Album_Detailes_Activity.EXTRA_DATA,
            Album_db(
                idAlbum = it.idAlbum,
                idArtist = it.idArtist,
                strAlbum = it.strAlbum,
                strArtistStripped = it.strArtistStripped,
                intYearReleased = it.intYearReleased,
                strGenre = it.strGenre,
                strAlbumThumb = it.strAlbumThumb,
                strDescriptionEN = it.strDescriptionEN
            )
        )
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
