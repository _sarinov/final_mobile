package com.example.artistlookup.Activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artistlookup.Adapter.TrackAdapter
import com.example.artistlookup.Networking.Track_Info_Loader
import com.example.artistlookup.R
import kotlinx.android.synthetic.main.activity_top_tracks.*

class Top_Tracks_Activity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "toptrack"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_tracks)
        setupTopTracks()
        setupAction()
    }


    private fun setupAction() {
        val actionBar = supportActionBar
        actionBar!!.title = "ArtistLookUp"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }


    private fun setupTopTracks() {
        Track_Info_Loader(onSuccess = { it ->
            topTrackList.layoutManager = LinearLayoutManager(this)
            topTrackList!!.adapter = TrackAdapter(trackDb = it.trackDb, onItemClick = {
                Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
            }, context = this)
        }, onError = {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }).loadTrack(intent.getStringExtra(EXTRA_DATA))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
