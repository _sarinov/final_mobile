package com.example.artistlookup.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.artistlookup.Database.Video_db
import com.example.artistlookup.R
import com.google.android.youtube.player.YouTubePlayer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_video.view.*


class VideoAdapter(
    private val videoDbs: List<Video_db> = listOf(),
    private val onItemClick: (Video_db) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<VideoAdapter.VideoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VideoViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_video, parent, false)
    )

    override fun getItemCount() = videoDbs.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bindArtist(videoDbs[position])
    }

    lateinit var youtubePlayerInit: YouTubePlayer.OnInitializedListener


    inner class VideoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindArtist(videos: Video_db) {
            Picasso.with(context).load(videos.strTrackThumb).into(view.trackImg)
            view.videoTitle.text = videos.strTrack
            view.setOnClickListener {
                onItemClick(videos)
            }
        }
    }
}
