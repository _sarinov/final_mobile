package com.example.artistlookup.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.artistlookup.Database.Album_db
import com.example.artistlookup.R
import kotlinx.android.synthetic.main.layout_album.view.*


class AlbumAdapter(
    private val albumDb: List<Album_db> = listOf(),
    private val onItemClick: (Album_db) -> Unit
) : RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlbumViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_album, parent, false)
    )

    override fun getItemCount() = albumDb.size

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bindAlbum(albumDb[position])
    }


    inner class AlbumViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindAlbum(albumDb: Album_db) {
            view.albumName.text = albumDb.strAlbum
            view.albumRelase.text = albumDb.intYearReleased
            view.albumGenre.text = albumDb.strGenre
            view.setOnClickListener {
                onItemClick(albumDb)
            }
        }
    }
}