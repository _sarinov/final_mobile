package com.example.artistlookup.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.artistlookup.Database.Favourites_db
import com.example.artistlookup.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_artist.view.*


class FavouriteAdapter(
    private val favourites: List<Favourites_db> = listOf(),
    private val context: Context
) : RecyclerView.Adapter<FavouriteAdapter.FavouritesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FavouritesViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_favourite, parent, false)
    )

    override fun getItemCount() = favourites.size

    override fun onBindViewHolder(holder: FavouritesViewHolder, position: Int) {
        holder.bindArtist(favourites[position])
    }


    inner class FavouritesViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindArtist(favouritesDb: Favourites_db) {

            Picasso.with(context).load(favouritesDb.strArtistThumb).into(view.artistImg)
            view.artistName.text = favouritesDb.strArtist
            view.artistGenre.text = favouritesDb.strGenre

        }
    }
}