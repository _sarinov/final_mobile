package com.example.artistlookup.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.artistlookup.Database.Track_db
import com.example.artistlookup.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_toptracks.view.*


class TrackAdapter(
    private val trackDb: List<Track_db> = listOf(),
    private val onItemClick: (Track_db) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<TrackAdapter.TrackViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TrackViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_toptracks, parent, false)
    )

    override fun getItemCount() = trackDb.size

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        holder.bindArtist(trackDb[position])
    }


    inner class TrackViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindArtist(trackDb: Track_db) {

            Picasso.with(context).load(trackDb.strTrackThumb).into(view.trackImg)
            view.albumName.text = trackDb.strAlbum
            view.trackName.text = trackDb.strTrack
            view.trackGenre.text = trackDb.strGenre
            view.trackDuration.text = trackDb.intDuration.toString()
            view.trackMood.text = trackDb.strMood

            view.setOnClickListener {
                onItemClick(trackDb)
            }
        }
    }
}