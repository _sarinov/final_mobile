package com.example.artistlookup.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.artistlookup.Database.Artist_db
import com.example.artistlookup.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_artist.view.*


class ArtistAdapter(
    private val artistDb: List<Artist_db> = listOf(),
    private val onItemClick: (Artist_db) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ArtistViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_artist, parent, false)
    )

    override fun getItemCount() = artistDb.size

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        holder.bindArtist(artistDb[position])
    }


    inner class ArtistViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindArtist(artistDb: Artist_db) {

            Picasso.with(context).load(artistDb.strArtistThumb).into(view.artistImg)
            view.artistName.text = artistDb.strArtist
            view.artistGenre.text = artistDb.strGenre

            view.setOnClickListener {
                onItemClick(artistDb)
            }
        }
    }
}