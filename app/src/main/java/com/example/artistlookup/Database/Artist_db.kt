package com.example.artistlookup.Database

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Artist_db(
    val idArtist: Int,
    val strArtist: String,
    val strGenre: String,
    val strArtistThumb: String,
    val strBiographyEN: String,
    val intBornYear: String,
    val strArtistLogo: String,
    val strWebsite: String
) : Parcelable {
    constructor(idArtistDb: List<Artist_db>) : this(0,"","","","","","","")
}

data class ArtistResponse(
    val artistDbs: List<Artist_db>
)

