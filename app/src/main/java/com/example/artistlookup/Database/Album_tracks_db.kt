package com.example.artistlookup.Database

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Album_track_db(
    val idArtist: Int,
    val strTrack: String,
    val intDuration: Int
) : Parcelable

data class AlbumTrackResponse(
    val trackdb: List<Album_track_db>
)

