package com.example.artistlookup.Networking

import com.example.artistlookup.Database.TrackResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Track_Info_Loader(
    private val onSuccess: (TrackResponse) -> Unit,
    private val onError: (Throwable) -> Unit
) {

    fun loadTrack(search: String) {
        Api_Factory.getApiArtist().getArtistTopTrack(search).enqueue(object : Callback<TrackResponse> {
            override fun onResponse(call: Call<TrackResponse>, response: Response<TrackResponse>) {
                onSuccess(response.body()!!)
            }
            override fun onFailure(call: Call<TrackResponse>, t: Throwable) {
                onError(t)
            }
        })
    }
}