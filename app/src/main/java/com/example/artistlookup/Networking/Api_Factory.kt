package com.example.artistlookup.Networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api_Factory {

    private const val REQUESTAPI = "https://theaudiodb.com/api/v1/json/1/"

    fun getArtistRetrofit(): Retrofit =
        Retrofit.Builder().baseUrl(REQUESTAPI).addConverterFactory(GsonConverterFactory.create()).build()

    fun getApiArtist(): Api_Client = getArtistRetrofit().create(Api_Client::class.java)
}
